/* Katie Eckart

    Tera Joyce, Chris Siemann

    .cpp file for class Cannonball
 
*/

#include "Cannonball.h"
#include "constants.h"
/*
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h" */


using namespace std;

Cannonball::Cannonball() // default constructor
{
}

void Cannonball::Clear() 
{
	for (int i = 0; i < xLocs.size(); i++) { // for all cannonballs created per level
	  xLocs.pop_front(); // deletes all x and y positions and velocities
	  yLocs.pop_front();
	  xSpeeds.pop_front();
	  ySpeeds.pop_front();
	}
}

void Cannonball::createCB(int lvl, int xloc) {

  //size = 10; // radius
 
 
  short xSpot, xSpeed, ySpeed;

      if (lvl == 1) {
      xSpot = rand() % (300) + (xloc-150); // completely random x location for easy level
      xSpeed = 0; // zero horizontal velocity for easy level
      ySpeed = rand() % 5 + 3; // small vertical velocity for easy level
      }
   
      if (lvl == 2) {
      xSpot = rand() % (200) + (xloc-100); // x location within +/-150 pixels of person
      xSpeed = 0; // zero horizontal velocity for medium level
      ySpeed = rand() % 8 + 4; // medium vertical velocity for medium level
      }
   
      if (lvl == 3) {
      xSpot = rand() % (100) + (xloc-50); // x location within +/-50 pixels of person
      xSpeed = 0; //rand() % 2 + 1; // easy horizontal velocity for hard level will be implemented later
      ySpeed = rand() % 10 + 6; // fast vertical velocity for hard level
      }
   
    
    xLocs.push_back(xSpot); // pushes back random x location for easy level onto xPosEasy deque
    yLocs.push_back(-10); // pushes back -10s since cannonball will begin at top of screen
    xSpeeds.push_back(xSpeed); // pushes back zeros since cannonballs fall only vertically on easy level
    ySpeeds.push_back(ySpeed); // pushes back random y velocities for easy level onto yVelEasy deque
 
}

int Cannonball::drawAllCBs(int lvl, SDL_Surface* screen){ // draws filled cannonball
   for (int k = 0; k < xLocs.size(); k++) // increments for all cannonballs that have been placed on screen
      {
         drawCircle(k, lvl, screen); // draws all cannonballs that have been placed on screen
         //Update the screen
         if( SDL_Flip(screen) == -1 ) { return 1; }
        }
        usleep(80000); // wait used for smooth graphic display
 
}

void Cannonball::drawCircle(int cbnumber, int lvl, SDL_Surface* screen) {

short radius = 10; // radius of cannonballs

// for loops for each level so that later on, for the hard level, the cannonballs can be falling diagonally

if (lvl = 1) { // draws for level 1
  filledEllipseRGBA(screen,
                      xLocs[cbnumber], yLocs[cbnumber],
                      radius, radius,
                      10, 20, 10, 255); // Uint8 r, Uint8 g, Uint8 b, Uint8 a
  yLocs[cbnumber] = yLocs[cbnumber] + ySpeeds[cbnumber]; // increments y position of cannonball based on speed
}

if (lvl = 2) { // draws for level 2
  filledEllipseRGBA(screen,
                      xLocs[cbnumber], yLocs[cbnumber],
                      radius, radius,
                      10, 20, 10, 255); // Uint8 r, Uint8 g, Uint8 b, Uint8 a
  yLocs[cbnumber] = yLocs[cbnumber] + ySpeeds[cbnumber]; // increments y position of cannonball based on speed            
}
if (lvl = 3) { // draws for level 3
  filledEllipseRGBA(screen,
                      xLocs[cbnumber], yLocs[cbnumber],
                      radius, radius,
                      10, 20, 10, 255); // Uint8 r, Uint8 g, Uint8 b, Uint8 a
  yLocs[cbnumber] = yLocs[cbnumber] + ySpeeds[cbnumber]; //increments y position of cannonball based on speed
  }
}

//checks if pirate has been hit
int Cannonball::checkHit(int xpirate, int ypirate, int cameraY) {

	for (int i = 0; i < xLocs.size(); i++) // for all cannonballs, checks if cannonball has hit pirate
	{
	  if ((xLocs[i] + 10 > xpirate) && (xLocs[i] - 10 < xpirate + 49) && ((yLocs[i] + 10 + cameraY) > ypirate) && ((yLocs[i] - 10 + cameraY) < ypirate + 120))
	  {
		return 1; // return if cannonball within space of pirate
	  }
	}
	
	return 0; // else returns 0 if not hit
}
   
