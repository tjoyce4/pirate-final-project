#include "Person.h"		// include base class header
#include "Captain.h"	// derived (this) class header
#include <string>
#include <iostream>

using namespace std;


Captain::Captain ()
{
height = 100;
width = 100;
xPosition = 100;
yPosition = 100;
}


Captain::Captain (int HT, int WDTH, int XPOS, int YPOS): Person (HT, WDTH, XPOS, YPOS)
{

}
  void  Captain::move() 
{
// Captain will be standing in his screen and will not need a move function
}

  void  Captain::display()
{
// Visual specific to Captain
}
