//Chris Siemann
//.cpp implementation file for Main

#include <iostream>
#include <vector>
#include <string>
#include "CrowNest.h"
#include "CapQuart.h"
#include "RopeClimb.h"
#include "PlankWalk.h"
#include "SDL/SDL.h"

int main()
{
	RopeClimb Page4; //display RopeClimb backdrop for 6 seconds
	CrowNest Page2; //display CrowNest backdrop for 6 seconds
	CapQuart Page1; //display CapQuart backdrop for 6 seconds
	PlankWalk Page3; //display PlankWalk backdrop for 6 seconds

	return 0;
}
