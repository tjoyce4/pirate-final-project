//Chris Siemann
//.h interface file for PlankWalk, inherited from Location
#ifndef PLANKWALK_H
#define PLANKWALK_H
#include "Person.h"
#include "SDL/SDL.h"

using namespace std;

class PlankWalk: public Person { 
	public:
		PlankWalk(); //constructor
		void move(); // moves plank pirate
   		void show(SDL_Surface*, SDL_Rect&, SDL_Surface*); //displays the set display for this location
   		void cleanUp(SDL_Surface*); // cleans up plank pirate screen
	private:
		//None
};

#endif
