#ifndef BUTTON_H
#define BUTTON_H
using namespace std;

class Button { 

  public: 
	//Initialize the variables 
	Button( int x, int y, int w, int h, SDL_Surface *ButtonImage, SDL_Surface *screen ); 
	
	//Handles events and set the button's sprite region void 
	int handle_events(SDL_Event&); 
	
	//Shows the button on the screen 
	void show(SDL_Surface *ButtonImage, SDL_Surface *screen);
	
	//The attributes of the button 
	SDL_Rect box; 
	
  private: 
 
};
#endif
