//Chris Siemann
//.h interface file for CrowNest, inherited from Location

#ifndef CROWNEST_H
#define CROWNEST_H
/*
#include <iostream>
#include <vector>
#include <string>
#include "SDL/SDL.h"
*/

using namespace std;

class CrowNest { //: public Location {
	public:
		void createPeople(); //creates people for this setting, namely the pirate atop the Nest
		void Display(); //displays the set display for this location
		int getSize(); //gets size of picture/pixels
		CrowNest(); //constructor
	private:
		int size;
};

#endif
