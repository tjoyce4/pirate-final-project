//Chris Siemann
//.h interface file for CapQuart, inherited from Location

#ifndef CAPQUART_H
#define CAPQUART_H
/*
#include <iostream>
#include <vector>
#include <string>
#include "SDL/SDL.h"*/

using namespace std;

class CapQuart { //: public Location {
    public:
        void createPeople(); //creates people for this setting, namely the captain
        void Display(); //displays the set display for this location
        CapQuart(); //constructor
    private:
        //None
};

#endif
