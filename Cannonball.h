/* Katie Eckart

    Tera Joyce, Chris Siemann

    .h file for creation of Cannonball class

*/

#ifndef CANNONBALL_H
#define CANNONBALL_H
#include <iostream>
#include <deque>
#include <string>
#include "SDL/SDL.h" 

using namespace std;

class Cannonball {

    public:
      Cannonball(); // non-default constructor (level)
      void Clear(); // deletes current deques of cannonball characteristics
      void createCB(int, int); // non-default constructor (level)
      void drawCircle(int, int, SDL_Surface*); // draws circle of cannonball (cbnumber, level)
      int drawAllCBs(int, SDL_Surface*); // draws filled cannonball (level)
      int checkHit(int, int, int); // checks to see if pirate is hit (cbnumber)
      int getSize(); // returns radius of cannonball
   
    private:
      int size; // radius of cannonball
      deque< short > xLocs; // deque of x positions for cannonballs
      deque< short > yLocs; // deque of y positions for cannonballs
      deque< int > xSpeeds; // deque of x velocities for cannonballs
      deque< int > ySpeeds; // deque of x velocities for cannonballs
   
};
#endif
