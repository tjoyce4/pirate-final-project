#ifndef PLANKPIRATE_H
#define PLANKPIRATE_H
#include "Person.h"


class PlankPirate:public Person
{

public:
  PlankPirate();
  PlankPirate (int, int, int, int);	// constructor
  void display ();		
  void move();

private:


};

#endif
