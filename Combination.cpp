/* Katie Eckart, Tera Joyce, Chris Siemann
 
 
*/

#include "Person.h"
#include "RopePirate.h"
#include "Cannonball.h"
#include "CapQuart.h"
#include "CrowNest.h"
#include "CapQuart.h"
#include "PlankWalk.h"
#include "Timer.h"
#include "Button.h"
#include "constants.h"

using namespace std;

//Initializes the surfaces
SDL_Surface* RopePirateImage = NULL;
SDL_Surface* PlankPirateImage = NULL;
SDL_Surface* background = NULL;
SDL_Surface* screen = NULL;

//The event structure
SDL_Event event;

//The camera
SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };


SDL_Surface* load_image( std::string filename )
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized surface that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized surface
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old surface
        SDL_FreeSurface( loadedImage );

        //If the surface was optimized
        if( optimizedImage != NULL )
        {
            //Color key surface
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
    }

    //Return the optimized surface
    return optimizedImage;
}

bool init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return false;
    }

    //Set the window caption
    SDL_WM_SetCaption( "Final Project - Fundamentals of Computing 2", NULL );

    //If everything initialized fine
    return true;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

bool load_files_Game()
{
    //Load the RopePirate image
    RopePirateImage = load_image( "pirateclimbing.bmp" );

    SDL_SetColorKey( RopePirateImage, SDL_SRCCOLORKEY, SDL_MapRGB(RopePirateImage->format, 218, 27, 225) ); // makes hot pink see through

    //Load the PlankPirate image
    PlankPirateImage = load_image( "pirateside.bmp" ); // Plank pirate image

    SDL_SetColorKey( PlankPirateImage, SDL_SRCCOLORKEY, SDL_MapRGB(PlankPirateImage->format, 75, 243, 16) ); //makes lime green see through

    //Load the background
    background = load_image( "realropeclimbactually.png" );

    //If there was a problem in loading the RopePirate
    if( RopePirateImage == NULL )
    {
        return false;
    }
    //If there was a problem in loading the PlankPirate
    if( PlankPirateImage == NULL )
    {
        return false;
    }


    //If there was a problem in loading the background
    if( background == NULL )
    {
        return false;
    }

    //If everything loaded fine
    return true;
}

void clean_up()
{
    //Free the surfaces
    SDL_FreeSurface( RopePirateImage );
    SDL_FreeSurface( PlankPirateImage );
    SDL_FreeSurface( background );

    //Quit SDL
    SDL_Quit();
}

int main() {

  CapQuart FirstImage; // Calls first intro screen
  RopePirate PirateCherie; // Creates the rope pirate used in the rope climb game
 
  Timer fps;

  int play = 0; // whether to play
  int level = 1; // current level of game // CHANGE to 2 or 3 to see game difficulty increase
  int hit = 0; // determines whether pirate is hit
  bool gameRunning = true; // determines whether game should continue
  bool quit = false; //if program is closed by user
  int levelUp = 0; // determines when to change levels
  int xloc = 0; // xloc of pirate
  int yloc = 0; // yloc of pirate
  int count = 0; // counts determine how often new cannonball should fall from screen
  int CameraLocation = 0; // camera location of visible screen

    //Initialize
    if( init() == false )
    {
        return 1;
    }

    //Load the files
    if( load_files_Game() == false )
    {
        return 1;
    }

  //While the user hasn't quit
 while( quit == false )
 {
     Cannonball Level;     

  while (gameRunning) // while game is running
   {

      
      while (hit == 0 && levelUp == 0)
       {
           //Start the frame timer
         fps.start();


        //If there's events to handle
          if (SDL_PollEvent(&event))
          {
            //Handle events for the Pirate
            PirateCherie.handle_input(event);

         if (event.type == SDL_QUIT)
         {
            gameRunning = false;
            levelUp = 1;
                     quit = true;
         }
          }

        //Move the RopePirate
        PirateCherie.move();

        //Set the camera
        CameraLocation = PirateCherie.set_camera(camera);

        //Show the background
        apply_surface( 0, 0, background, screen, &camera );

        //Show the pirate on the screen
        PirateCherie.show(RopePirateImage, camera, screen);

        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;
        }
	
	// gets x and y locations of PirateCherie
        xloc = PirateCherie.getX();
    	yloc = PirateCherie.getY(); 
    	
	//determines if pirate has hit cannonball
	hit = Level.checkHit(xloc, yloc, CameraLocation);

	//for level 1, cycles through 10 times before adding new cannonball
        if((count == 10) && (level == 1)) {
            Level.createCB(level, xloc);
             count = 0;
        }
        //for level 2, cycles through 8 times before adding new cannonball
        if((count == 8) && (level == 2)) {
            Level.createCB(level, xloc);
             count = 0;
        }
        //for level 3, cycles through 6 times before adding new cannonball
        if((count == 6) && (level == 3)) {
            Level.createCB(level, xloc);
             count = 0;
        }
        //draws all cannonballs currently stored
        Level.drawAllCBs(level, screen);
        
        count++; // increment count
	
	//if pirate has reached top of each level
        if (yloc == 3950 || yloc == 1940 || yloc == 20) 
        {
          levelUp = 1; // to increase level
          Level.Clear(); // clears cannonballs stored for each level
          count = 0;
        }
          
      }
    	levelUp = 0; // resets levelUp to 0 to re-enter while loop 
    	if (hit == 1) gameRunning = 0; // if pirate has been hit, ends the rope climb game
    	if (yloc == 20) gameRunning = 0; // if ylocation of pirate is at very top of rope climb, ends the rope climb game
    	level++; // increases level
    	
   }
  if (hit == 0)
  	CrowNest youWon; // if pirate wasn't hit, displays winning CrowNest picture
  else if(hit ==1) 
  {
 	PlankWalk youLost; // if pirate was hit, instantiates losing PlankWalk simulation
   	youLost.show(PlankPirateImage, camera, screen); // displays losing PlankWalk simulation
  }
  
  //Clean up
  clean_up(); 

  return 0; // ends program
}
}
