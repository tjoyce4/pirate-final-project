#include "constants.h"
#include "Button.h"
using namespace std;

Button::Button( int x, int y, int w, int h, SDL_Surface *ButtonImage, SDL_Surface *screen ) 
{ 

  //Set the button's attributes 
  
  box.x = x; 
  box.y = y; 
  box.w = w; 
  box.h = h; 
  
}

int Button::handle_events(SDL_Event &event) 
{ 
  //The mouse offsets 
  int x = 0;
  int y = 0;
 

  //If a mouse button was pressed 
  if( event.type == SDL_MOUSEBUTTONDOWN ) 
  { 
    //If the left mouse button was pressed 
    if( event.button.button == SDL_BUTTON_LEFT ) 
    { 
      //Get the mouse offsets 
      x = event.button.x; 
      y = event.button.y; 
      
      //If the mouse is over the button 
      if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) ) 
      { 
        //Set the button sprite 
        return 1; 
      } 
    } 
  }

   return 0;
}

//Show the button
void Button::show(SDL_Surface *ButtonImage, SDL_Surface *screen) 
{
  SDL_BlitSurface(ButtonImage, NULL, screen, &box); 
}
      
      
      
      
