//Chris Siemann
//.cpp implementation file for CapQuart

#include "constants.h"

#include <iostream>
#include <vector>
#include <string>
#include "CapQuart.h"
#include "SDL/SDL.h"
#include "Button.h" 

using namespace std;


CapQuart::CapQuart()
{
    Display(); //calls display in constructor
}

void CapQuart::createPeople()
{
    int xCappos; //arbitrary position of captain
    int yCappos;

    //Insert graphics here that creates captain at specified place on the screen
}

void CapQuart::Display()
{
    int keepGoing = 0; // determines whether display should continue 
    int x = 0; // x position of button
    int y = 0; // y position of button

    //The images
    SDL_Surface* IntroBackground = NULL;
    SDL_Surface* screen = NULL;
    SDL_Surface* ButtonImage = NULL;
    
    //Start SDL
    SDL_Init( SDL_INIT_EVERYTHING );
    //Set up screen
    screen = SDL_SetVideoMode( 900, 769, 32, SDL_SWSURFACE );
    //Load image
    IntroBackground = SDL_LoadBMP( "CapQuart.bmp" );
    ButtonImage = SDL_LoadBMP("button.bmp");

    //Apply image to screen
    SDL_BlitSurface(IntroBackground, NULL, screen, NULL );
    
    //Make the button 
    Button myButton( 170, 600, 100, 100, ButtonImage, screen );
    myButton.show(ButtonImage, screen);
    SDL_Event event;
    
    //Update Screen
    SDL_Flip( screen );
    while (keepGoing == 0)
    {
      while (SDL_PollEvent(&event))
      {
      //if (event.type == SDL_QUIT)
      	//return (-1);
      if(event.type == SDL_MOUSEBUTTONDOWN)
      {
      	x = event.button.x; // stores mouse click location
      	y = event.button.y;
      	if( ( x > myButton.box.x ) && ( x < myButton.box.x + myButton.box.w ) && ( y > myButton.box.y ) && ( y < myButton.box.y + myButton.box.h ) ) // if mouse click inside of button 
      	{
      	keepGoing = 1; //tells display to stop
      	}
        //keepGoing = myButton.handle_events(event);
      }
      }
    } 

    //Free the loaded image
    SDL_FreeSurface( IntroBackground );
    
}
