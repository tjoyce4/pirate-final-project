//Chris Siemann
//.h interface file for RopeClimb, inherited from Location

#ifndef ROPECLIMB_H
#define ROPECLIMB_H

#include <iostream>
#include <vector>
#include <string>
#include "SDL/SDL.h"

using namespace std;

class RopeClimb { //: public Location {
	public:
		void createPeople(); //creates people for this setting, namely the climbing pirate
		void Display(); //displays the set display for this location
		int getSize(); //gets size of picture/pixels
		RopeClimb(); //constructor
	private:
		int size;
};

#endif
