#ifndef PERSON_H
#define PERSON_H
#include "SDL/SDL.h"

using namespace std;


class Person
{

public:
  Person(); // default constructor for Person class
  Person(int, int, int, int); // non-default constructor for Person class (int xpos, int ypos, int xvels, int yvels)
  int getX(); //gets x value
  int getY(); //gets y value
  virtual void move() = 0; // pure virtual move() for Person class -- Rope pirate and Plank Walk pirate inherited 
  virtual void show(SDL_Surface*, SDL_Rect&, SDL_Surface*) = 0; // pure virtual show() to display image on screen

protected:
  int x; // x position of person in reference to full background screen
  int y; // y position
  int xVel; // x velocity
  int yVel; // y velocity

};

#endif
