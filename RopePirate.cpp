#include "Person.h"
#include "RopePirate.h"	// derived (this) class header
#include "constants.h"


using namespace std;

RopePirate::RopePirate(int x, int y, int xvel, int yvel):Person(x, y, xvel, yvel) // member initialization list syntax
{
}

RopePirate::RopePirate()
{
    //Initialize the offset x and y positions
    x = 450;
    y = 5810;

    //Initialize the velocity
    xVel = 0;
    yVel = 0;
    
}

void RopePirate::handle_input(SDL_Event &event)
{
    //If a key was pressed
    if( event.type == SDL_KEYDOWN )
    {
        //Adjust the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_UP: yVel -= RopePirateImage_HEIGHT / 4; break;
            case SDLK_DOWN: yVel -=  RopePirateImage_WIDTH / 100; break;
            case SDLK_LEFT: xVel -= RopePirateImage_WIDTH / 2; break;
            case SDLK_RIGHT: xVel += RopePirateImage_WIDTH / 2; break;
        }
    }
    
    //If a key was released
    else if( event.type == SDL_KEYUP )
    {
        //Adjust the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_UP: yVel += RopePirateImage_HEIGHT / 4; break;
            case SDLK_DOWN: yVel -=  RopePirateImage_WIDTH / 100; break;
            case SDLK_LEFT: xVel += RopePirateImage_WIDTH / 2; break;
            case SDLK_RIGHT: xVel -= RopePirateImage_WIDTH / 2; break;
        }
    }
}

void RopePirate::move()
{
    //Move the RopePirate left or right
    x += xVel;

    //If the RopePirate went too far to the left or right
    if( ( x < 0 ) || ( x + RopePirateImage_WIDTH > LEVEL_WIDTH ) )
    {
        //move back
        x -= xVel;
    }

    //Move the RopePirate up or down
    y += yVel;

    //If the RopePirate went too far up or down
    if( ( y < 0 ) || ( y + RopePirateImage_HEIGHT > LEVEL_HEIGHT ) )
    {
        //move back
        y -= yVel;
    }
}

void RopePirate::show(SDL_Surface *RopePirateImage, SDL_Rect &camera, SDL_Surface *screen)
{

    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x - camera.x;
    offset.y = y - camera.y;

    //Blit
    SDL_BlitSurface(RopePirateImage, NULL,  screen, &offset );

    //Show the RopePirate

}

int RopePirate::set_camera(SDL_Rect &camera)
{
    //Center the camera over the RopePirate
    camera.x = ( x + RopePirateImage_WIDTH / 2 ) - SCREEN_WIDTH / 2;
    //Put pirate at bottom of screen
    camera.y = ( y + RopePirateImage_HEIGHT / 2 ) - SCREEN_HEIGHT + 60 ;

    //Keep the camera in bounds.
    if( camera.x < 0 )
    {
        camera.x = 0;
    }
    if( camera.y < 0 )
    {
        camera.y = 0;
    }
    if( camera.x > LEVEL_WIDTH - camera.w )
    {
        camera.x = LEVEL_WIDTH - camera.w;
    }
    if( camera.y > LEVEL_HEIGHT - camera.h )
    {
        camera.y = LEVEL_HEIGHT - camera.h;
    }
    return camera.y;

}
