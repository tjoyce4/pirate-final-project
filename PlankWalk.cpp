//Chris Siemann
//.cpp implementation file for PlankWalk

#include "Person.h"
#include "PlankWalk.h"	// derived (this) class header
#include "constants.h"


using namespace std;

PlankWalk::PlankWalk()
{
    //Initialize the offsets
    x = 150;
    y = 297;

    //Initialize the velocity
    xVel = 3;
    yVel = 0;
}


void PlankWalk::show(SDL_Surface *PlankPirateImage, SDL_Rect &camera, SDL_Surface *GameScreen)
{
	//The images 
	SDL_Surface* hello = NULL; 
	SDL_Surface* screen = NULL;
	
	//Holds offsets
   	SDL_Rect offset;
	
	//Start SDL 
	SDL_Init( SDL_INIT_EVERYTHING ); 
	//Set up screen 
	screen = SDL_SetVideoMode( 900, 768, 32, SDL_SWSURFACE ); 
	//Load image 
	hello = SDL_LoadBMP( "PlankWalk.bmp" );
   	
	//Apply image to screen 
	SDL_BlitSurface( hello, NULL, screen, NULL ); 
	
	while(x < 400 || y < 700) {
  	//Get offsets
  	offset.x = x;
   	offset.y = y;
	//Moves plank pirate
	move();
	//displays background
	SDL_BlitSurface( hello, NULL, screen, NULL );
	//displays plank pirate
	SDL_BlitSurface(PlankPirateImage, NULL,  screen, &offset);
	SDL_Delay( 50 );
	//Update Screen 
	SDL_Flip( screen ); 
	}
	

	    

	//Pause 
	SDL_Delay( 3000 );
	//free used space
	cleanUp(hello);
	
	

}

void PlankWalk:: cleanUp(SDL_Surface *hello)
{
	//Free the loaded image 
	SDL_FreeSurface( hello ); 
	//Quit SDL 
	SDL_Quit(); 
}


void PlankWalk::move()
{

    x += xVel; // increments x and y positions based on velocities
    y += yVel;
    if (x >= 400) {
    	yVel = 5; // changes y velocity at end of plank to simulate falling pirate
    }

}

