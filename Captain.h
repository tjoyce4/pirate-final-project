#ifndef CAPTAIN_H
#define CAPTAIN_H
#include "Person.h"


class Captain:public Person
{


public:
  Captain();
  Captain (int, int, int, int);	// constructor
  void display ();		
  void move();

private:

};

#endif
