//Chris Siemann
//.cpp implementation file for RopeClimb

#include <iostream>
#include <vector>
#include <string>
#include "RopeClimb.h"
#include "SDL/SDL.h"
//include some sort of class that has logistics of Pirate moving

using namespace std;

RopeClimb::RopeClimb()
{
	Display(); //calls display in constructor
}

/*
void RopeClimb::createPeople()
{
	int RCxpos = getPirXpos(); //xpos of Pirate's center or torso
	int RCypos = getPirYpos(); //ypos of Pirate's center or torso
	int RCLarmx = getPirLarmXpos(); //xpos of Pirate's left arm
	int RCLarmy = getPirLarmYpos(); //ypos of Pirate's left arm
	int RCRarmx = getPirRarmXpos(); //xpos of Pirate's right arm
	int RCRarmy = getPirRarmYpos(); //ypos of Pirate's right arm
	int RCLlegx = getPirLlegXpos(); //xpos of Pirate's left leg
	int RCLlegy = getPirLlegYpos(); //ypos of Pirate's left leg
	int RCRlegx = getPirRlegXpos(); //xpos of Pirate's right leg
	int RCRlegy = getPirRlegYpos(); //ypos of Pirate's right leg

	//Insert graphics here that prints the climbing pirate and his body parts at the specified places from all of the get functions. Also, instead of all of the separate variables, we might just make a left/right side x pos variable and an arm/leg y pos variable
}
*/
void RopeClimb::Display()
{
		//The images 
	SDL_Surface* hello = NULL; 
	SDL_Surface* screen = NULL;

	//Start SDL 
	SDL_Init( SDL_INIT_EVERYTHING ); 
	//Set up screen 
	screen = SDL_SetVideoMode( 900, 825, 32, SDL_SWSURFACE ); 
	//Load image 
	hello = SDL_LoadBMP( "ropeclimb.bmp" );

	//Apply image to screen 
	SDL_BlitSurface( hello, NULL, screen, NULL ); 
	//Update Screen 
	SDL_Flip( screen ); 
	//Pause 
	SDL_Delay( 6000 );

	//Free the loaded image 
	SDL_FreeSurface( hello ); 

}

int RopeClimb::getSize()
{
	//return size;
}
