/* Katie Eckart

    Tera Joyce, Chris Siemann
   
    main.cpp file for Cannonball Class
   
*/

#include "Cannonball.h"
#include <iostream>
#include <deque>
#include <string>
#include <cstdlib>
#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

main() {

  int play = 0; // whether to play
  int level = 1; // current level of game
  int iMax = 15; // max cannonballs to avoid for level 1
  int i = 0;
  int hit = 0;
  int again;
 
  SDL_Init( SDL_INIT_VIDEO );
 
  SDL_Surface* screen = SDL_SetVideoMode( 900, 825, 0,
      SDL_HWSURFACE | SDL_DOUBLEBUF );
     
  SDL_WM_SetCaption( "Cannonball", 0 );
 
  SDL_Event event;
  bool gameRunning = true;
 
  while (gameRunning)
   {
      if (SDL_PollEvent(&event))
      {
         if (event.type == SDL_QUIT)
         {
            gameRunning = false;
         }
      }
     
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 120, 220, 220));
      level = 1;
      Cannonball Easy(level);
      Easy.drawAllCBs(level, screen);
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 120, 220, 220));
      usleep(1000000);
      level = 2;
      Cannonball Medium(level);
      Medium.drawAllCBs(level, screen);
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 120, 220, 220));
      usleep(1000000);
      level = 3;
      Cannonball Hard(level);
      Hard.drawAllCBs(level, screen);
     
     
      gameRunning = 0;
    }
    }
     
  /*
  cout << "Please enter 1 to play game." << endl;
  cin >> play;
 
  srand (time(NULL));
 
  while (play == 1){
    level = 1;
    if (level == 1) { // Easy Level
      Cannonball Easy(1);
      Easy.print();
      /*
        while (hit != 1)
        {
          hit = Easy.drawCBs(1);
         }
         
        }
   level = 2;
   if (level == 2) { // Medium Level
     Cannonball Medium(2);
     Medium.print();
     /*
         while (hit != 1)
         { /*
          hit = Medium.drawCBs(2);
         }
         
        }
   level = 3;
   if (level == 3) {
     Cannonball Hard(3);
     Hard.print();
     /*
     while (hit != 1)
     {
      hit = Hard.drawCBs(3);
     }
     
    }

    cout << "Would you like to play again? Press 1 to play again, or 0 to quit. " << endl;
    cin >> again;
    if (again == 1) play = 1;
    else play = 0;
}
*/
