#include "Person.h"
//#include "constants.h"

using namespace std;

  Person::Person()
{
   x =  100; //sets default x and y positions and velocities
   y =  100;
   xVel = 0;
   yVel = 0;

} 

Person::Person(int xpos, int ypos, int xvels, int yvels)
{
	//sets x and y positions and velocities
	x = xpos; 
	y = ypos;
	xVel = xvels;
	yVel = yvels;
}

//get functions for x and y positions
  int   Person::getX()
{
return x;
}

  int   Person::getY()
{
return y;
}


