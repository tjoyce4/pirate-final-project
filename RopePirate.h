#ifndef ROPEPIRATE_H
#define ROPEPIRATE_H
#include "Person.h"
#include "SDL/SDL.h"

class RopePirate:public Person
{
    public:
    
    RopePirate(int, int, int, int); // non-default constructor for RopePirate inherited class (int x, int y, int xvel, int yvel)
    
    //Initializes the variables
    RopePirate();

    //Takes key presses and adjusts the RopePirate's velocity
    void handle_input(SDL_Event&);

    //Moves the RopePirate
    void move();

    //Shows the RopePirate on the screen
    void show(SDL_Surface*, SDL_Rect&, SDL_Surface*);

    //Sets the camera over the RopePirate
    int set_camera(SDL_Rect&);
    
    private:
    //None

};


#endif
