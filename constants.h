#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <iostream>
#include <deque>
#include <string>
#include <cstdlib>
#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"
#include "SDL/SDL_image.h"

//Screen attributes
const int SCREEN_WIDTH = 940; 
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

//The frame rate
const int FRAMES_PER_SECOND = 20;

//The RopePirate dimensions
const int RopePirateImage_WIDTH = 49;
const int RopePirateImage_HEIGHT = 120;

//The RopePirate dimensions
const int PlankPirateImage_WIDTH = 98;
const int PlankPirateImage_HEIGHT = 142;

const int ButtonImage_WIDTH = 100;
const int ButtonImage_HEIGHT = 100;

//The dimensions of the level
const int LEVEL_WIDTH = 940;
const int LEVEL_HEIGHT = 5990;

#endif
